#include <stdio.h>

int i=5; // numeri globali
int main()
{

/*
	int i=7;// numeri locali
	
    printf("%d ",i);//=7 mi da il numero locali come risultato
    
*/	


/*
	int i= 7; 
	
	for(int i=0;i<5;i++)
	printf("%d ",i);  //=4 ciclo fino ad numero globali che assegnato fuori dal main e riconosce il valore 5
	printf("\n");
	printf("%d",i);  //=7 la seconda stampa invece mi stampa il numero locale
*/	
	


/*	
      int i=7;
     {int i= 8; }
	  printf("%d ",i);//qua mi stampa il numero locale e non globale e non 8 perche il compilatore conosce il numero locale 
	  //poi invece il otto non stampa perche c'e un blocco che sarrebbe il parenthesi grafe che fa bloccare ad utilizzare diciamo
*/	
	
	
/*	
	{
	
	 int i=7;
     {int i= 8; }
	
    }
    printf("%d ",i);// qua stmpa inve il numero globale ="5" perche tutti i numeri locale sono sotto grafe che vouldire un blocco
*/    

	
}
